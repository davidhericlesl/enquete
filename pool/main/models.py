import datetime
from django.db import models
from django.utils import timezone

# Create your models here.

class Question(models.Model):
	text = models.CharField(max_length = 200)
	datepub = models.DateTimeField('Publish Date')
	def __str__(self):
	    return self.text
def published_recently(self):
    return self.datepub>=timezone.now()-datetime.timedelta(days=1)

class Options(models.Model):
	text = models.CharField(max_length = 200)
	votes = models.IntegerField(default = 0)
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	def __str__(self):
	    return self.text